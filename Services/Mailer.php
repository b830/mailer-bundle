<?php


namespace App\Akip\MailerBundle\Services;


use App\Akip\EshopBundle\Entity\Customer;
use App\Akip\EshopBundle\Entity\Order;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class Mailer
{
    const TEMPLATE_REGISTER = 'register';
    const TEMPLATE_ORDER_SUMMARY = 'orderSummary';
    const TEMPLATE_RESET_PASSWORD = 'resetPassword';
    const TEMPLATE_ORDER_STATUS_UPDATE = 'updateOrderStatus';
    const TEMPLATE_ORDER_PAYMENT_CONFIRM = 'updateOrderPaymentConfirm';

    private $mailer;


    /**
     * Mailer constructor.
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param $templateName
     * @param $emailTo = [] / string
     * @param $subject
     * @param $templateParams = ['parameterName' => $parameterValue]
     * @param string $locale
     */
    public function sendMail($templateName, $emailTo, $subject, $templateParams = [], $locale = 'cs')
    {
        $email = (new TemplatedEmail())
            ->from($_ENV['FROM_EMAIL'])
            ->subject($subject);
        if (is_array($emailTo)) {
            foreach ($emailTo as $item) {
                $email->addTo($item);
            }
        } else {
            $email->to($emailTo);
        }
        switch ($templateName) {
            case self::TEMPLATE_REGISTER:
                $email->htmlTemplate('@AkipMailer/email/register/registration.html.twig')
                    ->textTemplate('@AkipMailer/email/register/registration.txt.twig');
                break;
            case self::TEMPLATE_ORDER_SUMMARY:
                $templateParams['protocol'] = $_ENV['PROTOCOL'];
                $templateParams['host'] = $_ENV['HOST_NAME'];
                $email->htmlTemplate('@AkipMailer/email/demand/summary/summary.html.twig')
                    ->textTemplate('@AkipMailer/email/demand/summary/summary.txt.twig');
                $email->attachFromPath('docs/obchodni-podminky.pdf', 'Obchodní podmínky', 'application/pdf');
                break;
            case self::TEMPLATE_RESET_PASSWORD:
                $email->htmlTemplate('@AkipMailer/email/reset_password/email.html.twig')
                    ->textTemplate('@AkipMailer/email/reset_password/email.txt.twig');
                break;
            case self::TEMPLATE_ORDER_STATUS_UPDATE:
                $email->htmlTemplate('@AkipMailer/email/demand/status_update.html.twig')
                    ->textTemplate('@AkipMailer/email/demand/status_update.txt.twig');
                break;
            case self::TEMPLATE_ORDER_PAYMENT_CONFIRM:
                $email->htmlTemplate('@AkipMailer/email/demand/payment_confirm/confirm.html.twig')
                    ->textTemplate('@AkipMailer/email/demand/payment_confirm/confirm.txt.twig');
        }

        $templateParams['locale'] = $locale;
        $templateParams['subject'] = $subject;
        $email->context($templateParams);

        $this->mailer->send($email);
    }

}
